/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

"use strict";

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	ADAPT		= true;
const	STROKE_SOFT	= 0.2;
const	STROKE_HARD	= 1;

const	TO_CM		= 1 / 2.54;

const	CM			= 59;			/// SIZE OF A PIXEL

let	ANGLE			= 0;			/// ANGLE OF CAMERA
let	DPI				= 100;			/// DOT PER INCH
let	WIDTH			= 21 * CM;		/// WIDTH OF DOCUMENT
let	HEIGHT			= 29.7 * CM;	/// HEIGHT OF DOCUMENT
let	COV				= 17 * CM;		/// CONE OF VISION DIAMETER
let	LENGTH			= 5 * CM;		/// SIZE OF SQUARE
let	LINES			= 500;			/// NUMBER OF LINES TO DRAW
let	PACK			= 10;			/// LENGTH OF A PACK OF SQUARE
let	UNIT;
let	RATIO;

class	Line {
	constructor(x1, y1, x2, y2) {
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
	}

	intersect(line, draw = false) {
		let		x1, y1, x2, y2, x3, y3, x4, y4;
		let		x, y;

		x1 = this.x1;
		y1 = this.y1;
		x2 = this.x2;
		y2 = this.y2;
		x3 = line.x1;
		y3 = line.y1;
		x4 = line.x2;
		y4 = line.y2;
		x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4))
		/ ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
		y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4))
		/ ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
		if (draw == true) {
			ellipse(x, y, 10);
		}
		return ({"x": x, "y": y});
	}

	reach(line) {
		let		inter;

		inter = this.intersect(line);
		this.x2 = inter.x;
		this.y2 = inter.y
	}

	draw(mirror = false) {
		line(this.x1, this.y1, this.x2, this.y2);
		if (mirror == true) {
			line(this.x1, this.y1, this.x2, HEIGHT / 2 - (this.y2 - HEIGHT / 2));
		}
	}
}

let		line_horizon = new Line(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
let		line_down = new Line(0, HEIGHT, WIDTH, HEIGHT);
let		pr, pl;		/// VANISHING LEFT AND RIGHT POINTS
let		pov;		/// POINT OF VIEW
let		angle_slider;
let		pack_slider;
let		line_slider;
let		length_input;
let		width_input;
let		height_input;
let		cov_input;
let		dpi_slider;
let		unit_radio;
let		take_screenshot;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function	download() {
	save("perspective_grid_" + ANGLE + ".png");
}

function	update_env() {
	let		unit, ratio, dpi;

	/// UPDATE UNIT IF CHANGED
	unit = unit_radio.value();
	if (UNIT != unit) {
		ratio = (unit == "px") ? 1 : ((unit == "in") ? DPI : DPI * TO_CM);
		width_input.value((width_input.value() * RATIO) / ratio);
		height_input.value((height_input.value() * RATIO) / ratio);
		cov_input.value((cov_input.value() * RATIO) / ratio);
		length_input.value((length_input.value() * RATIO) / ratio);
		RATIO = (unit == "px") ? 1 : ((unit == "in") ? DPI : DPI * TO_CM);
		UNIT = unit;
	}
	/// UPDATE DPI IF CHANGED
	dpi = dpi_slider.value();
	if (DPI != dpi) {
		DPI = dpi;
		RATIO = (unit == "px") ? 1 : ((unit == "in") ? DPI : DPI * TO_CM);
	}
	/// UPDATE DIMENSIONS IF CHANGED
	if (WIDTH != width_input.value() || HEIGHT != height_input.value()) {
		WIDTH = width_input.value() * RATIO;
		HEIGHT = height_input.value() * RATIO;
		line_horizon = new Line(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
		line_down = new Line(0, HEIGHT, WIDTH, HEIGHT);
		resizeCanvas(WIDTH, HEIGHT);
	}
	/// UPDATE ENV
	ANGLE = angle_slider.value();
	LENGTH = length_input.value() * RATIO;
 	COV = cov_input.value() * RATIO;
	LINES = line_slider.value();
	PACK = pack_slider.value();
}

function	draw_extra() {
	/// DRAW HORIZON LINE
	stroke(255, 0, 0);
	strokeWeight(4);
	line(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
	/// DRAW AREA
	stroke(0, 255, 0);
	line(WIDTH / 2 - COV / 2, 0, WIDTH / 2 - COV / 2, HEIGHT);
	line(WIDTH / 2 + COV / 2, 0, WIDTH / 2 + COV / 2, HEIGHT);
	/// INFO
	if (take_screenshot == false) {
		stroke(0);
		strokeWeight(1);
		textSize(32);
		text("angle: " + ANGLE, 10, 32);
		text("square size", 10, 64);
		text("width", 10, 99);
		text("height", 10, 134);
		text("COV", 10, 169);
		text("lines: " + LINES, 10, 204);
		text("pack: " + PACK, 10, 239);
		text("unit:", 350, 32);
		text("dpi: " + DPI, 350, 64);
	} else {
		download();
		take_screenshot = false;
	}
}

function	draw_side_grid(a, b, c, d, middle, point) {
	let		i;
	let		inter, inter2;
	let		guide;
	let		line;

	for (i = 2; i < LINES; ++i) {
		if (i % PACK == 0) {
			strokeWeight(map(i, 0, LINES, STROKE_HARD, 0));
		} else {
			strokeWeight(map(i, 0, LINES, STROKE_SOFT, 0));
		}
		/// CREATE GUIDE
		inter = a.intersect(b);
		inter2 = c.intersect(middle);
		guide = new Line(inter.x, inter.y, inter2.x, inter2.y);
		/// NEW LINE
		inter = guide.intersect(d);
		line = new Line(point, HEIGHT / 2, inter.x, inter.y);
		line.reach(line_down);
		line.draw(true);
		/// OFFSET
		a = c;
		c = line;
	}
}

//////////////////////////////////////////////////
/// P5
//////////////////////////////////////////////////

function	setup() {
	let		button;

	UNIT = "px";
	RATIO = 1;
	if (ADAPT == true) {
		WIDTH = windowWidth;
		HEIGHT = windowHeight;
		line_horizon = new Line(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
		line_down = new Line(0, HEIGHT, WIDTH, HEIGHT);
	}
	createCanvas(WIDTH, HEIGHT);
	/// ANGLE SLIDER
	angle_slider = createSlider(-44, 44, 0, 1);
	angle_slider.position(180, 10);
	/// LENGTH INPUT
	length_input = createInput(LENGTH);
	length_input.position(180, 45);
	/// WIDTH INPUT
	width_input = createInput(WIDTH);
	width_input.position(180, 80);
	/// HEIGHT INPUT
	height_input = createInput(HEIGHT);
	height_input.position(180, 115);
	/// COV INPUT
	cov_input = createInput(COV);
	cov_input.position(180, 150);
	/// DPI INPUT
	dpi_slider = createSlider(72, 600, DPI);
	dpi_slider.position(480, 45);
	/// POINT SLIDER
	line_slider = createSlider(10, 1000, LINES);
	line_slider.position(180, 185);
	/// PACK SLIDER
	pack_slider = createSlider(1, 50, PACK);
	pack_slider.position(180, 220);
	/// UNIT RADIO
	unit_radio = createRadio();
	unit_radio.option("px");
	unit_radio.option("in");
	unit_radio.option("cm");
	unit_radio.value("px");
	unit_radio.position(480, 10);
	/// DOWNLOAD BUTTON
	button = createButton("Download");
	button.position(350, 80);
	button.mousePressed(function () {take_screenshot = true;});
	take_screenshot = false;
}

function	draw() {
	let		side_a, side_b, side_c, side_d;
	let		l1, l2;
	let		square1, square2;
	let		guide1, guide2;
	let		inter, inter2;
	let		diagonal1, diagonal2;
	let		center;
	let		middle_left, middle_right;
	let		h;

	update_env();
	/// BEGIN CALCULATION
	clear();
	stroke(0);
	/// CONE OF VISION AND VANISHING POINTS
	pov = (sqrt(3) / 2) * COV;
	h = HEIGHT / 2 + pov;
	pl = WIDTH / 2 - (tan((45 - ANGLE) * (PI / 180)) * pov);
	pr = WIDTH / 2 + (tan((45 + ANGLE) * (PI / 180)) * pov);
	/// GET SQUARE SIDES A AND B
	strokeWeight(STROKE_HARD);
	side_a = new Line(pl, HEIGHT / 2, WIDTH / 2, h);	
	side_b = new Line(pr, HEIGHT / 2, WIDTH / 2, h);
	side_a.reach(line_down);
	side_b.reach(line_down);
	side_a.draw(true);
	side_b.draw(true);
	/// CREATE BASE SQUARE
	square1 = {
		"x": WIDTH / 2 - cos((45 + ANGLE) * (PI / 180)) * LENGTH,
		"y": HEIGHT / 2 - sin((45 + ANGLE) * (PI / 180)) * LENGTH
	};
	square2 = {
		"x": WIDTH / 2 - cos((135 + ANGLE) * (PI / 180)) * LENGTH,
		"y": HEIGHT / 2 - sin((135 + ANGLE) * (PI / 180)) * LENGTH
	};
	/// BUILD GUIDES
	guide1 = new Line(square1.x, square1.y, WIDTH / 2, HEIGHT / 2 + pov);
	inter = guide1.intersect(line_horizon);
	guide1 = new Line(inter.x, inter.y, inter.x, h);
	guide2 = new Line(square2.x, square2.y, WIDTH / 2, HEIGHT / 2 + pov);
	inter = guide2.intersect(line_horizon);
	guide2 = new Line(inter.x, inter.y, inter.x, h);
	/// GET SQUARE SIDE C AND D
	if (PACK > 1) {
		strokeWeight(STROKE_SOFT);
	}
	inter = guide1.intersect(side_a);
	side_d = new Line(pr, HEIGHT / 2, inter.x, inter.y);
	side_d.reach(line_down);
	side_d.draw(true);
	inter = guide2.intersect(side_b);
	side_c = new Line(pl, HEIGHT / 2, inter.x, inter.y);
	side_c.reach(line_down);
	side_c.draw(true);
	/// GET DIAGONALS
	inter = side_d.intersect(side_c);
	inter2 = side_a.intersect(side_b);
	diagonal1 = new Line(inter.x, inter.y, inter2.x, inter2.y);
	inter = side_b.intersect(side_c);
	inter2 = side_a.intersect(side_d);
	diagonal2 = new Line(inter.x, inter.y, inter2.x, inter2.y);
	/// GET MIDDLE LINES
	center = diagonal1.intersect(diagonal2);
	middle_left = new Line(pl, HEIGHT / 2, center.x, center.y);
	middle_left.reach(line_down);
	middle_right = new Line(pr, HEIGHT / 2, center.x, center.y);
	middle_right.reach(line_down);
	/// BUILD GRID INSIDE RIGHT
	draw_side_grid(side_a, side_b, side_c, side_d, middle_right, pl);
	/// BUILD GRID INSIDE LEFT
	draw_side_grid(side_b, side_a, side_d, side_c, middle_left, pr);
	/// BUILD GRID EXTERN RIGHT
	draw_side_grid(side_c, side_d, side_a, side_b, middle_right, pl);
	/// BUILD GRID EXTERN LEFT
	draw_side_grid(side_d, side_c, side_b, side_a, middle_left, pr);
	/// DRAW EXTRA
	draw_extra();
}
