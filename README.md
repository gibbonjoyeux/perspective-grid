# Perspective grid

This is a 2 points perspective generator.

Change the parameters as you want and download the result as a '.png' image.

I made this program for my own drawing purpose :)
The area between green guides is the one you can draw on. Outside area can be
too distorted.

This project, built with p5js, is generated without any 3D calculation. As I am
not that good in math, I built that generator with the old school grid drawing
method on paper :)

## Parameters
- angle: rotation angle of the camera
- unit: unit used for dimensions
- dpi: dpi (dots per inch) used for `in` and `cm` units
- square size: size of a square in the grid
- width: width of the document
- height: height of the document
- cov: diameter of the cone of vision. Basically, it is the width of you drawing
area
- lines (points): number of lines the draw
- length packed: dimension of pack squares (darken ones)

## Test it online

You do not have to download the project, you can use it online on
[https://codepen.io/gibbonjoyeux/full/zYvZwKL](https://codepen.io/gibbonjoyeux/full/zYvZwKL)
:)
